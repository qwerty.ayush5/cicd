import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, Pressable, Image, Button } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
export default function App() {
  return (
    <View>
      <Cat name="Bahadur" />
      <Cat name="Rani" />
    </View>
  );
}

const Cat = (props) => {

  const [hungry, setHungry] = useState(true);

  return (
    <View>
      <Text> Hello, I am {props.name} and I am {hungry ? 'hhungry' : 'full'}</Text>
      <Button onPress={() => { setHungry(false) }} disabled={!hungry} title={hungry ? "Give me milk" : "thanks"} />
    </View>
  );
}